package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserDTOService;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;
import ru.tsc.denisturovsky.tm.repository.dto.ProjectDTORepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static ru.tsc.denisturovsky.tm.constant.ContextTestData.CONTEXT;
import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.*;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class ProjectDTORepositoryTest {

    @NotNull
    private final static IUserDTOService USER_SERVICE = CONTEXT.getBean(IUserDTOService.class);

    @NotNull
    private final static ProjectDTORepository REPOSITORY = CONTEXT.getBean(ProjectDTORepository.class);

    @NotNull
    private static String USER_ID = "";

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        System.out.println(user.getId());
        if (user != null) USER_SERVICE.remove(user);
    }

    @Test
    public void addByUserId() throws Exception {
        USER_PROJECT3.setUserId(USER_ID);
        Assert.assertNotNull(REPOSITORY.save(USER_PROJECT3));
        @Nullable final Optional<ProjectDTO> project = REPOSITORY.findByUserIdAndId(USER_ID, USER_PROJECT3.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT3.getId(), project.get().getId());
        Assert.assertEquals(USER_ID, project.get().getUserId());
    }

    @After
    public void after() throws Exception {
        REPOSITORY.deleteAll();
    }

    @Before
    public void before() throws Exception {
        USER_PROJECT1.setUserId(USER_ID);
        USER_PROJECT2.setUserId(USER_ID);
        REPOSITORY.save(USER_PROJECT1);
        REPOSITORY.save(USER_PROJECT2);
    }

    @Test
    public void clearByUserId() throws Exception {
        REPOSITORY.deleteByUserId(USER_ID);
        Assert.assertEquals(0, REPOSITORY.findByUserId(USER_ID).size());
    }

    @Test
    public void createByUserId() throws Exception {
        USER_PROJECT3.setUserId(USER_ID);
        @NotNull final ProjectDTO project = REPOSITORY.save(USER_PROJECT3);
        Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
        Assert.assertEquals(USER_ID, project.getUserId());
    }

    @Test
    public void createByUserIdWithDescription() throws Exception {
        USER_PROJECT3.setUserId(USER_ID);
        @NotNull final ProjectDTO project = REPOSITORY.save(USER_PROJECT3);
        Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
        Assert.assertEquals(USER_PROJECT3.getDescription(), project.getDescription());
        Assert.assertEquals(USER_ID, project.getUserId());
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertFalse(REPOSITORY.existByUserIdAndId(USER_ID, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(REPOSITORY.existByUserIdAndId(USER_ID, USER_PROJECT1.getId()));
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertEquals(Collections.emptyList(), REPOSITORY.findByUserId(""));
        final List<ProjectDTO> projects = REPOSITORY.findByUserId(USER_ID);
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        projects.forEach(project -> Assert.assertEquals(USER_ID, project.getUserId()));
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertEquals(Optional.empty(), REPOSITORY.findByUserIdAndId(USER_ID, NON_EXISTING_PROJECT_ID));
        @Nullable final Optional<ProjectDTO> project = REPOSITORY.findByUserIdAndId(USER_ID, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.get().getId());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertEquals(2, REPOSITORY.findByUserId(USER_ID).size());
    }

    @Test
    public void removeByUserId() throws Exception {
        REPOSITORY.deleteByUserIdAndId(USER_ID, USER_PROJECT2.getId());
        Assert.assertEquals(Optional.empty(), REPOSITORY.findByUserIdAndId(USER_ID, USER_PROJECT2.getId()));
    }

    @Test
    public void update() throws Exception {
        USER_PROJECT1.setName(USER_PROJECT3.getName());
        REPOSITORY.save(USER_PROJECT1);
        Assert.assertEquals(
                USER_PROJECT3.getName(), REPOSITORY.findByUserIdAndId(USER_ID, USER_PROJECT1.getId()).get().getName());
    }

}