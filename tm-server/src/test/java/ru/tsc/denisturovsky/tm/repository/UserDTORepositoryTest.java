package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.service.IServerPropertyService;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserDTOService;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;
import ru.tsc.denisturovsky.tm.repository.dto.UserDTORepository;
import ru.tsc.denisturovsky.tm.util.HashUtil;

import java.util.Optional;

import static ru.tsc.denisturovsky.tm.constant.ContextTestData.CONTEXT;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserDTORepositoryTest {

    @NotNull
    private final static IUserDTOService USER_SERVICE = CONTEXT.getBean(IUserDTOService.class);

    @NotNull
    private static final IServerPropertyService PROPERTY_SERVICE = CONTEXT.getBean(
            IServerPropertyService.class);

    @NotNull
    private final static UserDTORepository REPOSITORY = CONTEXT.getBean(UserDTORepository.class);

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = USER_SERVICE.add(USER_TEST);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
    }

    @Test
    public void add() throws Exception {
        Assert.assertNotNull(REPOSITORY.save(ADMIN_TEST));
        @Nullable final Optional<UserDTO> user = REPOSITORY.findById(ADMIN_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST.getId(), user.get().getId());
    }

    @After
    public void after() throws Exception {
        @Nullable UserDTO user = USER_SERVICE.findByLogin(ADMIN_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
    }

    @Test
    public void create() throws Exception {
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(ADMIN_TEST_LOGIN);
        user.setPasswordHash(HashUtil.salt(PROPERTY_SERVICE, ADMIN_TEST_PASSWORD));
        REPOSITORY.save(user);
        Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void createWithEmail() throws Exception {
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(ADMIN_TEST_LOGIN);
        user.setPasswordHash(HashUtil.salt(PROPERTY_SERVICE, ADMIN_TEST_PASSWORD));
        user.setEmail(ADMIN_TEST_EMAIL);
        REPOSITORY.save(user);
        Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(ADMIN_TEST.getEmail(), user.getEmail());
    }

    @Test
    public void createWithRole() throws Exception {
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(ADMIN_TEST_LOGIN);
        user.setPasswordHash(HashUtil.salt(PROPERTY_SERVICE, ADMIN_TEST_PASSWORD));
        user.setRole(Role.ADMIN);
        REPOSITORY.save(user);
        Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(REPOSITORY.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(REPOSITORY.existsById(USER_TEST.getId()));
    }

    @Test
    public void findByEmail() throws Exception {
        @Nullable final UserDTO user = REPOSITORY.findByEmail(USER_TEST.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
    }

    @Test
    public void findByLogin() throws Exception {
        @Nullable final UserDTO user = REPOSITORY.findByLogin(USER_TEST.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertEquals(Optional.empty(), REPOSITORY.findById(NON_EXISTING_USER_ID));
        @Nullable final Optional<UserDTO> user = REPOSITORY.findById(USER_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.get().getId());
    }

    @Test
    public void isEmailExists() throws Exception {
        Assert.assertTrue(REPOSITORY.isEmailExists(USER_TEST.getEmail()));
    }

    @Test
    public void isLoginExists() throws Exception {
        Assert.assertTrue(REPOSITORY.isLoginExists(USER_TEST.getLogin()));
    }

    @Test
    public void removeByUserId() throws Exception {
        REPOSITORY.save(ADMIN_TEST);
        Assert.assertNotNull(REPOSITORY.findById(ADMIN_TEST.getId()));
        REPOSITORY.delete(ADMIN_TEST);
        Assert.assertEquals(Optional.empty(), REPOSITORY.findById(ADMIN_TEST.getId()));
    }

}