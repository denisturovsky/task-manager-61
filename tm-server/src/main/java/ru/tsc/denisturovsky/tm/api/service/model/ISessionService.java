package ru.tsc.denisturovsky.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

    boolean existsById(
            @Nullable String userId,
            @Nullable String id
    ) throws Exception;

}

