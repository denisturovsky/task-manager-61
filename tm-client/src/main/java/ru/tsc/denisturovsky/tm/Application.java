package ru.tsc.denisturovsky.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.denisturovsky.tm.component.ClientBootstrap;
import ru.tsc.denisturovsky.tm.configuration.ClientConfiguration;

public final class Application {

    public static void main(@Nullable String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ClientConfiguration.class);
        @NotNull final ClientBootstrap clientBootstrap = context.getBean(ClientBootstrap.class);
        clientBootstrap.run(args);
    }

}
