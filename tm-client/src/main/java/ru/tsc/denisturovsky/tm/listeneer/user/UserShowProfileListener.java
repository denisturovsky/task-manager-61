package ru.tsc.denisturovsky.tm.listeneer.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.dto.request.UserViewProfileRequest;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.event.ConsoleEvent;

@Component
public final class UserShowProfileListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "Show user info";

    @NotNull
    public static final String NAME = "user-show-profile";

    @Override
    @EventListener(condition = "@userShowProfileListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest(getToken());
        @Nullable final UserDTO user = authEndpoint.viewProfileUser(request).getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
