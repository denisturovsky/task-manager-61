package ru.tsc.denisturovsky.tm.listeneer.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.denisturovsky.tm.dto.request.UserUpdateProfileRequest;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.event.ConsoleEvent;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

@Component
public final class UserUpdateListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "Update user";

    @NotNull
    public static final String NAME = "user-update";

    @Override
    @EventListener(condition = "@userUpdateListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER PROFILE UPDATE]");
        System.out.println("[ENTER FIRST NAME:]");
        @NotNull String firstName = TerminalUtil.nextLine();
        System.out.println("[ENTER LAST NAME:]");
        @NotNull String lastName = TerminalUtil.nextLine();
        System.out.println("[ENTER MIDDLE NAME:]");
        @NotNull String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken());
        request.setFirstName(firstName);
        request.setLastName(lastName);
        request.setMiddleName(middleName);
        userEndpoint.updateProfileUser(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
